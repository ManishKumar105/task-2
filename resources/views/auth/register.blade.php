<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
         integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" 
         crossorigin="anonymous">
         <title>Register</title>
    </head>
    <body>
       <div class="container">
           <div class="row" style="margin-top:45px;margin-left:350px;">
                <div class="col-md-6 col-md-offset-2">
                    <h4 class="text-center">Registration</h4><hr>
                    <form action="{{route('save')}}" method="POST">

                        @if(Session::get('success'))
                        <div class="alert alert-success">
                            <li>{{Session::get('success')}}</li>
                        </div>
                        @endif

                        @if(Session::get('fail'))
                        <div class="alert alert-danger">
                            <li>{{Session::get('fail')}}</li>
                        </div>
                        @endif

                        @csrf
                    <div class="form-group">
                            <label>Employee Id</label>
                            <input type="text" class="form-control" name="employee_id" placeholder="Enter id" value="{{old('employee_id')}}">
                            <span class="text-danger">@error('employee_id'){{ $message }} @enderror</span>
                        </div>
                         <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter your name" value="{{old('name')}}">
                            <span class="text-danger">@error('name'){{ $message }} @enderror</span>
                        </div>
                         <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="Enter phone number" value="{{old('phone')}}">
                            <span class="text-danger">@error('phone'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Enter email address" value="{{old('email')}}">
                            <span class="text-danger">@error('email'){{ $message }} @enderror</span>
                        </div>
                         <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter password" value="{{old('password')}}">
                            <span class="text-danger">@error('password'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>User Type</label>
                                <select name="user_type" id="user_type" class="form-control">
                                     <option value="">Select</option>
                                <option value="Manager">Manager</option>
                                <option value="Employee">Employee</option>
                            </select>
                            <span class="text-danger">@error('user_type'){{ $message }} @enderror</span>
                        </div>
                        

                        
                        <button type="submit" class="btn btn-block btn-primary">Sign Up</button><br>
                        <a href="{{url('login')}}">I already have an account, Sign In</a>
                    </form>
                </div>
           </div>
       </div>
    </body>
</html>